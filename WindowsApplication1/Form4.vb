Public Class Form4
    Dim whitelist() As String
    Private Sub Form4_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        whitelist = System.IO.File.ReadAllLines("white-list.txt")
        For i As Integer = 0 To whitelist.Length - 1
            ListBox1.Items.Add(whitelist(i))
        Next
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Microsoft.VisualBasic.Asc(e.KeyChar) = 13 Then
            If TextBox1.Text > "" Then
                ListBox1.Items.Add(TextBox1.Text)
            End If
            TextBox1.Text = ""
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        ListBox1.Items.Remove(ListBox1.SelectedItem)
    End Sub

    Private Sub Button2_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        ReDim whitelist(ListBox1.Items.Count - 1)
        For i As Integer = 0 To ListBox1.Items.Count - 1
            whitelist(i) = ListBox1.Items(i)
        Next
        System.IO.File.WriteAllLines("white-list.txt", whitelist)
        Me.Close()
    End Sub

    Private Sub Button1_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If TextBox1.Text > "" Then
            ListBox1.Items.Add(TextBox1.Text)
        End If
        TextBox1.Text = ""
    End Sub

    Private Sub listBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ListBox1.KeyDown
        If e.KeyCode = Keys.Delete Then
            ListBox1.Items.Remove(ListBox1.SelectedItem)
        End If
    End Sub
End Class