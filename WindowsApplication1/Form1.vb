Imports System
Imports System.Net
Public Class Form1
    Public fileDetail As IO.FileInfo, jarsize As Integer

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        config_text()

        'backup
        Try
            My.Computer.FileSystem.CreateDirectory("backup")
        Catch ex As Exception
        End Try
        Try
            System.IO.File.Copy("server.properties", "backup\[" + TimeOfDay.Date.ToString + "]server.properties")
        Catch ex As Exception
        End Try

        'RUN
        System.IO.File.WriteAllText("server.properties", FULLYtext)
        Try
            Process.Start("C:\Program Files (x86)\Java\jre6\bin\java.exe", "-Xincgc -Xmx1G -jar craftbukkit-0.0.1-SNAPSHOT.jar")
        Catch ex As Exception
            MsgBox("Bukkit will be downloaded!", MsgBoxStyle.OkOnly)
            tb.Text = "Bukkit is updating..."
            Dim wc As New System.Net.WebClient
            wc.DownloadFile("http://ci.bukkit.org/job/dev-CraftBukkit/lastSuccessfulBuild/artifact/target/craftbukkit-0.0.1-SNAPSHOT.jar", "craftbukkit-0.0.1-SNAPSHOT.jar")
            fileDetail = My.Computer.FileSystem.GetFileInfo("craftbukkit-0.0.1-SNAPSHOT.jar")
            If IntPtr.Size > 4 Then

                psi.FileName = "%ProgramFiles(x86)%\Java\jre6\bin\java.exe"
                System.Diagnostics.Process.Start(psi, "-Xincgc -Xmx1G -jar craftbukkit-0.0.1-SNAPSHOT.jar")
                'Process.Start("%ProgramFiles(x86)%\Java\jre6\bin\java.exe", "-Xincgc -Xmx1G -jar craftbukkit-0.0.1-SNAPSHOT.jar")
                '64 Bit
            Else

                Process.Start("%ProgramFiles%\Java\jre6\bin\java.exe", "-Xincgc -Xmx1G -jar craftbukkit-0.0.1-SNAPSHOT.jar")
                '32 Bit!
            End If
        End Try
    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
        TextBox1.Enabled = False : TextBox2.Enabled = False : TextBox3.Enabled = False : TextBox4.Enabled = False
        ComboBox1.Enabled = True
        ComboBox1.Items.Clear()
        For i As Integer = 0 To Dns.GetHostEntry(Dns.GetHostName).AddressList.Length - 1
            ComboBox1.Items.Add(Dns.GetHostEntry(Dns.GetHostName).AddressList(i).ToString())
        Next i
        Dim index As Integer = 0
        Do While index < ComboBox1.Items.Count
            Dim value As String = ComboBox1.Items(index)
            If value.Contains(":") Then
                ComboBox1.Items.RemoveAt(index)
            Else
                index = index + 1
            End If
        Loop
        ComboBox1.SelectedIndex = 0
        Button1.Enabled = True
    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        TextBox1.Enabled = True : TextBox2.Enabled = True : TextBox3.Enabled = True : TextBox4.Enabled = True
        ComboBox1.Enabled = False
        Button1.Enabled = True
    End Sub

#Region "check_ip"
    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged
        If TextBox1.Text > 255 Then tb.Text = "Wrong IP adress!" Else tb.Text = ""
        If TextBox1.Text.Length = 3 Then TextBox2.Focus()
    End Sub

    Private Sub TextBox2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox2.TextChanged
        If TextBox2.Text > 255 Then tb.Text = "Wrong IP adress!" Else tb.Text = ""
        If TextBox2.Text.Length = 3 Then TextBox3.Focus()
    End Sub

    Private Sub TextBox3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox3.TextChanged
        If TextBox3.Text > 255 Then tb.Text = "Wrong IP adress!" Else tb.Text = ""
        If TextBox3.Text.Length = 3 Then TextBox4.Focus()
    End Sub

    Private Sub TextBox4_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox4.TextChanged
        If TextBox4.Text > 255 Then tb.Text = "Wrong IP adress!" Else tb.Text = ""
    End Sub
#End Region

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Form2.ShowDialog()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        config_text()
        Form3.ShowDialog()
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            readedConfig = System.IO.File.ReadAllLines("server.properties")
            ' Form4.ShowDialog()

            TextBox5.Text = readedConfig(2).Replace("level-name=", "")
            If readedConfig(3) = "hellworld=false" Then CheckBox1.Checked = False Else CheckBox1.Checked = True
            If readedConfig(4) = "spawn-monsters=false" Then CheckBox2.Checked = False Else CheckBox2.Checked = True
            If readedConfig(5) = "online-mode=false" Then CheckBox3.Checked = False Else CheckBox3.Checked = True
            If readedConfig(6) = "spawn-animals=false" Then CheckBox4.Checked = False Else CheckBox4.Checked = True
            NumericUpDown1.Value = readedConfig(7).Replace("max-players=", "")
            RadioButton2.Checked = True : ComboBox1.SelectedItem = readedConfig(8).Replace("server-ip=", "")
            If readedConfig(9) = "pvp=false" Then CheckBox5.Checked = False Else CheckBox5.Checked = True
            NumericUpDown3.Value = readedConfig(11).Replace("server-port=", "")
            If readedConfig(13) = "white-list=false" Then CheckBox6.Checked = False Else CheckBox6.Checked = True
            NumericUpDown2.Value = readedConfig(14).Replace("spawn-protection=", "")
        Catch ex As Exception
        End Try

    End Sub
    Public Function config_text()
        Dim world, hell, monsters, online, animals, pvp, whitelist, ip, slots, protection, port As String

        'World
        world = "level-name=" + TextBox5.Text

        'HELL
        If CheckBox1.Checked = True Then hell = "hellworld=true" Else hell = "hellworld=false"

        'Monsters
        If CheckBox2.Checked = True Then monsters = "spawn-monsters=true" Else monsters = "spawn-monsters=false"

        'Online
        If CheckBox3.Checked = True Then online = "online-mode=true" Else online = "online-mode=false"

        'Animals
        If CheckBox4.Checked = True Then animals = "spawn-animals=true" Else animals = "spawn-animals=false"

        'PvP
        If CheckBox5.Checked = True Then pvp = "pvp=true" Else pvp = "pvp=false"

        'Whitelist
        If CheckBox6.Checked = True Then whitelist = "white-list=true" Else whitelist = "white-list=false"

        'Slots
        slots = "max-players=" + NumericUpDown1.Value.ToString

        'IP
        If RadioButton1.Checked = True Then ip = "server-ip=" + TextBox1.Text + "." + TextBox2.Text + "." + TextBox3.Text + "." + TextBox4.Text _
        Else If RadioButton2.Checked = True Then ip = "server-ip=" + ComboBox1.SelectedItem

        'Protection
        protection = "spawn-protection=" + NumericUpDown2.Value.ToString

        'Port
        port = "server-port=" + NumericUpDown3.Value.ToString

        'FULL
        FULLYtext = "#Minecraft server properties [Created by Bukkit SS v" + Application.ProductVersion + "]" + vbCrLf _
        + "#" + TimeOfDay.DayOfWeek.ToString + TimeOfDay + vbCrLf _
        + world + vbCrLf _
        + hell + vbCrLf _
        + monsters + vbCrLf _
        + online + vbCrLf _
        + animals + vbCrLf _
        + slots + vbCrLf _
        + ip + vbCrLf _
        + pvp + vbCrLf _
        + "level-seed=" + vbCrLf _
        + port + vbCrLf _
        + "allow-flight=false" + vbCrLf _
        + whitelist + vbCrLf _
        + protection

    End Function

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        tb.Text = "Bukkit is updating..."

        Dim wc As New System.Net.WebClient
        wc.DownloadFile("http://ci.bukkit.org/job/dev-CraftBukkit/lastSuccessfulBuild/artifact/target/craftbukkit-0.0.1-SNAPSHOT.jar", "craftbukkit-0.0.1-SNAPSHOT.jar")
        fileDetail = My.Computer.FileSystem.GetFileInfo("craftbukkit-0.0.1-SNAPSHOT.jar")
        jarsize = fileDetail.Length
        Timer1.Start()

    End Sub


    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Dim jarsize2 As Integer = fileDetail.Length
        If jarsize2 = jarsize Then
            tb.Text = "Bukkit updated!"
            Timer1.Stop()
            Timer2.Start()
        Else

        End If

    End Sub

    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        tb.Text = ""
        Timer2.Stop()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        config_text()
        'backup
        Try
            My.Computer.FileSystem.CreateDirectory("backup")
        Catch ex As Exception
        End Try
        Try
            System.IO.File.Copy("server.properties", "backup\[" + TimeOfDay.Date.ToString + "]server.properties")
        Catch ex As Exception
        End Try


        'save
        System.IO.File.WriteAllText("server.properties", FULLYtext)
    End Sub

    Private Sub CheckBox6_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox6.CheckedChanged
        If CheckBox6.Checked = True Then Button6.Enabled = True Else Button6.Enabled = False
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Form4.Show()
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Form5.Show()
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        'SendKeys.Send()
        Form6.Show()

    End Sub



End Class
