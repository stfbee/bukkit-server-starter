Imports Ionic.Zip
Public Class F6plugins
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            ListBox1.Items.Add(ListBox2.SelectedItem)
            ListBox2.Items.Remove(ListBox2.SelectedItem)
        Catch ex As Exception
        End Try
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        move_add()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        For i As Integer = 0 To ListBox1.Items.Count - 1
            ListBox1.SetSelected(i, True)
        Next
        move_add()
    End Sub
    Public Function move_add()
        Try
            Dim a As Integer = ListBox1.SelectedItems.Count
            For i As Integer = 1 To a
                Dim link(), fname() As String
                Dim wc As New System.Net.WebClient

                If ListBox1.SelectedItem = "Essentials" Then
                    ReDim link(5), fname(5)
                    link(0) = "http://earth2me.net:8002/guestAuth/repository/download/bt21/.lastSuccessful/Essentials.jar" : fname(0) = "Essentials.jar"
                    link(1) = "http://earth2me.net:8002/guestAuth/repository/download/bt21/.lastSuccessful/EssentialsProtect.jar" : fname(1) = "EssentialsProtect.jar"
                    link(2) = "http://earth2me.net:8002/guestAuth/repository/download/bt21/.lastSuccessful/EssentialsSpawn.jar" : fname(2) = "EssentialsSpawn.jar"
                    link(3) = "http://earth2me.net:8002/guestAuth/repository/download/bt21/.lastSuccessful/EssentialsChat.jar" : fname(3) = "EssentialsChat.jar"
                    link(4) = "http://earth2me.net:8002/guestAuth/repository/download/bt21/.lastSuccessful/EssentialsGroupManager.jar" : fname(4) = "EssentialsGroupManager.jar"
                    link(5) = "http://earth2me.net:8002/guestAuth/repository/download/bt21/.lastSuccessful/EssentialsGroupBridge.jar" : fname(5) = "EssentialsGroupBridge.jar"
                ElseIf ListBox1.SelectedItem = "Big Brother" Then
                    ReDim link(0), fname(0)
                    link(0) = "http://ci.nexisonline.net:8080/job/BigBrother/Recommended/artifact/target/BigBrother.jar" : fname(0) = "BigBrother.jar"
                ElseIf ListBox1.SelectedItem = "CraftBook" Then
                    ReDim link(1), fname(1)
                    link(0) = "https://github.com/downloads/sk89q/craftbook/craftbook-3.0-alpha2.zip" : fname(0) = "craftbook-3.0-alpha2.zip"
                    link(1) = "https://github.com/downloads/sk89q/worldedit/worldedit-4.4.zip" : fname(1) = "worldedit-4.4.zip"
                End If

                bar2.Maximum = a : bar2.Value = i
                bar.Maximum = link.Length + 1

                ListBox2.Items.Add(ListBox1.SelectedItem)
                ListBox1.Items.Remove(ListBox1.SelectedItem)

                For val As Integer = 0 To link.Length Step +1
                    bar.Value = val + 1
                    wc.DownloadFile(link(val), "plugins\" + fname(val))
                    If fname(val).Contains(".zip") Then
                        Using zip1 As ZipFile = ZipFile.Read("plugins\" + fname(val))
                            For Each ze As ZipEntry In zip1
                                ze.Extract("plugins", ExtractExistingFileAction.OverwriteSilently)
                            Next
                        End Using
                        System.IO.File.Delete("plugins\" + fname(val))
                    End If

                Next

            Next
        Catch ex As Exception
        End Try
    End Function

    Public Function move_remove()
        Dim a As Integer = ListBox2.SelectedItems.Count
        For i As Integer = 1 To a


            Dim wc As New System.Net.WebClient

            If ListBox1.SelectedItem = "Essentials" Then
                System.IO.File.Move("plugins\Essentials.jar", "plugins\plugins\Essentials.jar")
            ElseIf ListBox1.SelectedItem = "Big Brother" Then
                System.IO.File.Move("plugins\BigBrother.jar", "plugins\plugins\BigBrother.jar")
            ElseIf ListBox1.SelectedItem = "CraftBook" Then
                System.IO.File.Move("plugins\CraftBook.jar", "plugins\plugins\CraftBook.jar")
                System.IO.File.Move("plugins\CraftBookMechanisms.jar", "plugins\plugins\CraftBookMechanisms.jar")
                System.IO.File.Move("plugins\CraftBookCircuits.jar", "plugins\plugins\CraftBookCircuits.jar")
                System.IO.File.Move("plugins\CraftBookVehicles.jar", "plugins\plugins\CraftBookVehicles.jar")
                System.IO.File.Move("plugins\worldedit.jar", "plugins\plugins\worldedit.jar")
            End If
            ListBox1.Items.Add(ListBox2.SelectedItem)
            ListBox2.Items.Remove(ListBox2.SelectedItem)

        Next
    End Function

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If System.IO.Directory.Exists("plugins\plugins") = False Then
            System.IO.Directory.CreateDirectory("plugins\plugins")
        End If

        move_remove()
    End Sub
End Class